#test

Test project for experimenting with website content hosted on GitLab. Not intended to be used for anything other than me messing around with the contents to see how they work/appear.

## License
For the sake of completeness, this is licensed under the MIT license which as I understand it means that you can do whatever you want with it provided you include a copy of said license.

In reality, the contents of this project are more or less a web development scratchpad, so you are probably better of just starting from scratch and hacking something together yourself like I did.
